// var DEBUG = true;
// DEBUG = false;
var config = require('./config.json');
var MongoClientModule = require('mongodb').MongoClient;
var MongoServer = require('mongodb').Server;
var database = null;
var collections = ["groupInfo"];
var MongoClient = new MongoClientModule(new MongoServer(config.MongoDBAddress, config.MongoServerPort));
var MongoCollectionSet = config.DBCollection;
MongoClient.open(function(err, mongoClient) {
	if (err) {
		console.log("DB open error: " + err);
		return;
	}
	database = mongoClient.db(config.MongoDatabaseName);

});

// User stuff

exports.deleteUserWithUserName = function (userName, callback) {
	database.collection(MongoCollectionSet.users).remove({"userName": userName}, function (err, result) {
		callback(err);
	});
};

exports.deleteAllUsers = function (callback) {
	database.collection(MongoCollectionSet.users).remove({}, function (err, result) {
		callback(err);
	});
};

exports.authStudent = function (groupID, userName, callback) {
	if (!database) {
		callback(new Error("database offline"));
		return;
	}
	if (!userName || !groupID) {
		callback(new Error("Missing parameters"));
		return;
	}

	database.collection(MongoCollectionSet.groupInfo).find({"groupID": groupID}).toArray(function (err, result) {
		if (err) {
			callback(new Error("Error reading from database"));
			return;
		}
		if (result.length > 0) {
			database.collection(MongoCollectionSet.users).insert({
				"userName": userName,
				"groupID": groupID
			}, function (err, result) {
				if (err) {
					callback(err);
					return;
				}
				callback(null);
			});
		} else {
			callback(new Error("group " + groupID + " not exist"));
		}
	});
};

exports.getUserInfo = function (userName, callback) {
	database.collection(MongoCollectionSet.users).find({"userName": userName}).toArray(function (err, result) {
		if (err) {
			callback(err, null);
			return;
		}
		if (result.length != 1) {
			callback(new Error("result faulty"), null);
			return;
		}
		callback(null, result[0]._id);
	});
};

// path stuff

exports.insertPath = function (userID, userName, groupID, pathID, path, callback) {
	var newPkg = {
		"userID": userID,
		"userName": userName,
		"groupID": groupID,
		"pathID": path.pathID,
		"path": path,
		"timestamp": new Date()
	};
	var historyPkg = {
		"action": "add",
		"userID": userID,
		"userName": userName,
		"groupID": groupID,
		"pathID": path.pathID,
		"path": path,
		"timestamp": newPkg.timestamp
	};
	database.collection(MongoCollectionSet.pathHistory).insert(historyPkg, function (err, result) {
		if (err) {
			callback(err, null);
			return;
		}
		database.collection(MongoCollectionSet.path).insert(newPkg, function (err, result) {
			if (err) {
				callback(err, null);
				return;
			}
			callback(null, newPkg);
		});
	});
};

exports.undoPath = function (userID, userName, groupID, pathID, callback) {
	var historyPkg = {
		"action": "undo",
		"userID": userID,
		"userName": userName,
		"groupID": groupID,
		"pathID": pathID,
		"timestamp": new Date()
	};
	database.collection(MongoCollectionSet.pathHistory).insert(historyPkg, function (err, result) {
		if (err) {
			callback(err);
			return;
		}
		database.collection(MongoCollectionSet.path).remove({"pathID": pathID}, function (err, result) {
			if (err) {
				callback(err);
				return;
			}
			callback(null);
		});
	});
};

exports.clearCanvas = function (userID, userName, groupID, callback) {
	var historyPkg = {
		"action": "clear",
		"userID": userID,
		"userName": userName,
		"groupID": groupID,
		"timestamp": new Date()
	};
	database.collection(MongoCollectionSet.pathHistory).insert(historyPkg, function (err, result) {
		if (err) {
			callback(err);
			return;
		}
		database.collection(MongoCollectionSet.path).remove({}, function (err, result) {
			if (err) {
				callback(err);
				return;
			}
			callback(null);
		});
	});
};

exports.loadHistory = function (groupID, callback) {
	database.collection(MongoCollectionSet.path).find({"groupID": groupID}).toArray(function (err, result) {
		if (err) {
			callback(err, null);
			return;
		}
		callback(null, result);
	});
};

exports.setImage = function (userID, userName, groupID, fileName, callback) {
	var historyPkg = {
		"action": "setImage",
		"userID" : userID,
		"userName": userName,
		"groupID": groupID,
		"fileName": fileName,
		"timestamp": new Date()
	};
	// console.log(historyPkg);
	database.collection(MongoCollectionSet.pathHistory).insert(historyPkg, function (err, result) {
		if (err) {
			callback(err);
			return;
		}
		// console.log("in insert db");
		callback(null);
	});
};

//Teacher's stuff

exports.createGroup = function (groupID, groupName, callback) {
	database.collection(MongoCollectionSet.groupInfo).insert({
		"groupID": groupID,
		"name": groupName
	}, function (err, result) {
		if (err) {
			callback(err);
			return;
		}
		callback(null);
	});
};

exports.getImageHistory = function (groupID, callback) {
	database.collection(MongoCollectionSet.pathHistory).find({
		"action": "setImage",
		"groupID": groupID
	}).toArray(function (err, result) {
		if (err) {
			callback(err, null);
		} else {
			if (result.length > 0) callback(null, result[result.length - 1].fileName);
			else callback(null, "0");
		}
	});
};

exports.getStudentList = function (groupID, callback) {
	database.collection(MongoCollectionSet.users).find({
		"groupID": groupID
	}).toArray(function (err, result) {
		if (err) {
			callback(err, []);
			return;
		}
		callback(null, result);
	});
};

exports.getGroupList = function (callback) {
	database.collection(MongoCollectionSet.groupInfo).find({}).toArray(function (err, result) {
		if (err) {
			callback(err, []);
			return;
		}
		callback(null, result);
	});
};
