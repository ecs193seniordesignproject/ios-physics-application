var express  = require('express');
var http = require('http');
var sio = require('socket.io');
var path = require('path');
var studentSocketHandler = require('./studentSocketHandler.js');
var teacherSocketHandler = require('./teacherSocketHandler.js');
var staticHandler = require('./staticHandler.js');

var app = express();
var phyServer = http.createServer(app);
phyServer.listen(8080);
var io = sio.listen(phyServer);

io.enable('browser client minification');
io.enable('browser client etag');
io.enable('browser client gzip');
io.set('log level', 1);


app.use("/bg", express.static(path.join(__dirname, "./bg")));


//SocketIO connection handlers start from here

teacherSocketHandler.handle(io.of('/teacher'));
studentSocketHandler.handle(io.of('/student'), teacherSocketHandler);

var staticServer = express();
staticServer.listen(8088);

//Set up CORS on ajax server since app domain is localhost
var allowCrossDomain = function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	next();
};
staticServer.configure(function () {
	staticServer.use(allowCrossDomain);
});

//Serving ajax requests
staticServer.get("/getGroupList", function (request, response) {
	staticHandler.getGroupList(function (err, result) {
		if (!err) response.send(result);
		else response.send([]);
	});
});
staticServer.get("/getStudentList", function (request, response) {
	var groupID = parseInt(request.query.groupID);
	console.log("groupID = " + groupID);
	if (!groupID) {
		response.send([]);
		return;
	}
	staticHandler.getStudentList(groupID, function (err, result) {
		if (!err) response.send(result);
		else response.send([]);
	});
});
