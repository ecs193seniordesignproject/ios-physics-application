var c = require("./cont.js");
var config = require('./config.json');
exports.teacher = null;
var DEBUG = config.DEBUG;
// DEBUG = false;

var MongoUtil = require('./MongoUtil.js');

exports.handle = function (teacher) {
	exports.teacher = teacher;

	teacher.on("connection", function (socket) {
		if (DEBUG) console.log(socket.id + " connected as teacher");
		socket.on(c.event.teacherSetUpGroupEvent, function (data, fn) {
			setUpGroupEvent(socket, data.groupID, data.groupName, fn);
		});
		loadHistory(function (err, result) {
			if (err) {
				if (DEBUG) console.log("teacherSocketHandler::handle::" + err);
				return;
			}
			socket.emit(c.event.teacherLoadHistoryEvent, result);
		});
		socket.on(c.event.teacherLoadLastBackgroundEvent, function (data, fn) {
			loadLastImageList(function (err, result) {
				if (err) {
					if (DEBUG) console.log("teacherSocketHandler::teacherLoadLastBackgroundEvent::" + err);
					fn({});
				} else {
					fn(result);
				}
			});
		});
	});
};

exports.studentHasStartedPath = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentStartPathEvent, data);
};
exports.studentSendCoordEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentSendCoordinateEvent, data);
};

exports.studentSetImageEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentSetImageEvent, data);
};

exports.studentEndPathEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentEndPathEvent, data);
};

exports.studentUndoEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentUndoPathEvent, data);
};

exports.studentClearEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentClearCanvasEvent, data);
};

exports.studentJoinEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentJoinEvent, data);
};

exports.studentLeaveEvent = function (studentSocket, data) {
	console.log("teacher: " + JSON.stringify(data));
	exports.teacher.emit(c.event.studentLeaveEvent, data);
};

var setUpGroupEvent = function (socket, groupID, groupName, fn) {
	MongoUtil.createGroup(groupID, groupName, function (err) {
		if (err) {
			if (DEBUG) console.log("teacherSocketHandler::setUpGroupEvent::" + err);
			fn('0');
			return;
		}
		fn('1');
	});
};

var loadHistory = function (callback) {
	MongoUtil.getGroupList(function (err, result) {
		if (err) {
			callback(err, null);
			return;
		}
		var allResult = {};
		var counter = 0;
		var finalTrigger = function (ftErr, ftGroupID, ftResult) {
			counter++;
			if (ftErr) {
				if (DEBUG) console.log("teacherSocketHandler::loadHistory::finalTrigger::" + ftErr);
				return;
			}
			allResult[ftGroupID] = ftResult;
			if (counter == result.length) callback(null, allResult);
		};
		for (var i = 0; i < result.length; i++) {
			(function (myI) {
				console.log("loading " + result[myI].groupID);
				MongoUtil.loadHistory(String(result[myI].groupID), function (err, loopResult) {
					if (err) {
						if (DEBUG) console.log("teacherSocketHandler::loadHistory::" + err);
						finalTrigger(err, null, null);
						return;
					}
					finalTrigger(null, result[myI].groupID, loopResult);
				});
			})(i);
		}
	});
};

var loadLastImageList = function (callback) {
	MongoUtil.getGroupList(function (err, result) {
		if (err) {
			callback(err, null);
			return;
		}
		var allResult = {};
		var counter = 0;
		var finalTrigger = function (ftErr, ftGroupID, ftResult) {
			counter++;
			if (ftErr) {
				if (DEBUG) console.log("teacherSocketHandler::loadHistory::finalTrigger::" + ftErr);
				return;
			}
			allResult[ftGroupID] = ftResult;
			if (counter == result.length) callback(null, allResult);
		};
		for (var i = 0; i < result.length; i++) {
			(function (myI) {
				console.log("loading " + result[myI].groupID);
				MongoUtil.getImageHistory(String(result[myI].groupID), function (err, loopResult) {
					if (err) {
						if (DEBUG) console.log("teacherSocketHandler::loadHistory::" + err);
						finalTrigger(err, null, null);
						return;
					}
					finalTrigger(null, result[myI].groupID, loopResult);
				});
			})(i);
		}
	});
};
