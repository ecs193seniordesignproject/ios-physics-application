exports.students = null;
exports.teacher = null;
var util = require('util');
var c = require('./cont.js');
var config = require('./config.json');
var DEBUG = config.DEBUG;
var clone = require('clone');
var MongoUtil = require('./MongoUtil.js');


//Group Ops
var groupList = {};

var enlistSocketToGroup = function (socketID, groupID) {
	if (!groupList[groupID]) {
		groupList[groupID] = [socketID];
	} else {
		groupList[groupID].push(socketID);
	}
	if (DEBUG) {
		console.log(util.format("Group[%s] is now %s"), groupID, groupList[groupID]);
		console.log(groupList[groupID]);
	}
};

var deleteSocketFromGroup = function (socketID) {
	console.log("in delete socketFromGroup");
	for (var key in groupList) {
		if (groupList.hasOwnProperty(key)) {
			if (groupList[key].indexOf(socketID) > -1) {
				if (DEBUG) {
					console.log(util.format("Removing socket %s from group %s", socketID, key));
				}
				console.log("before remove");
				console.log(groupList[key]);
				groupList[key].splice([groupList[key].indexOf(socketID)], 1);
				console.log("after remove");
				console.log(groupList[key]);
			}
		}
	}
};

var getSocketListWithGroupID = function (groupID) {
	if (!groupList[groupID]) return [];
	else return groupList[groupID];
};

var getGroupCountWithID = function (groupID) {
	if (!groupList[groupID] || !(groupInfo[groupID] instanceof Array)) return 0;
	else return groupList[groupID].length;
};

//Register table
var IDPairs = {};
var IDUserNameParis = {};

var enlistUser = function (socketID, userID, userName, callback) {
	if (DEBUG) console.log(util.format("Enlisting User %s with userName %s and userID %s", socketID, userName, userID));
	IDPairs[socketID] = userID;
	IDUserNameParis[socketID] = userName;
	if (callback) callback();
};

var deleteUser = function (socketID, callback) {
	if (DEBUG) console.log(util.format("Deleting %s: %s: %s", socketID, IDPairs[socketID], IDUserNameParis[socketID]));
	if (IDUserNameParis[socketID]) {
		MongoUtil.deleteUserWithUserName(IDUserNameParis[socketID], function (err) {
			if (err && DEBUG) {
				if (DEBUG) console.log(err);
				return;
			}
			delete IDPairs[socketID];
			delete IDUserNameParis[socketID];
			deleteSocketFromGroup(socketID);
			if (callback) callback();
		});
	}
};

var dumpAllUser = function (callback) {
	MongoUtil.deleteAllUsers(function (err) {
		if (err && DEBUG) {
			console.log(err);
			return;
		}
		if (callback) callback();
	});
};

var getGroupMembers = function(socketID, groupID) {
	var groupListMy = clone(getSocketListWithGroupID(groupID));
	// console.log("target group before trim self");
	// console.log(groupListMy);
	var selfIndex = groupListMy.indexOf(socketID);
	if (selfIndex > -1) {
		// delete groupListMy[selfIndex];
		groupListMy.splice(selfIndex, 1);
	}
	// console.log("target group after trim self");
	// console.log(groupListMy);
	return groupListMy;
};

exports.handle = function (students, teacherHandler) {
	exports.students = students;
	exports.teacher = teacherHandler;

	students.authorization(function (handshakeData, cb) {
		if (DEBUG) console.log('Auth: ', handshakeData.query);
		var userName = handshakeData.query.userName;
		var groupID = parseInt(handshakeData.query.groupID);
		MongoUtil.authStudent(groupID, userName, function (err) {
			if (err) {
				if (DEBUG) console.log("studentSocketHandler::handle::" + err);
				cb(err, false);
			} else {
				cb(null, true);
			}
		});
	});

	students.on("connection", function (socket) {
		console.log(socket.id + " connected as student");
		socket.state = c.socketState.connected;

		socket.on(c.event.studentGetHistoryEvent, function (data, fn) {
			loadHistoryPathEv(socket, data, fn);
		});
		socket.on(c.event.studentRequestInfoEvent, function (data, fn) {
			requestInfoEv(socket, data, fn);
		});
		socket.on(c.event.studentStartPathEvent, function (data, fn) {
			startPathEv(socket, data, fn);
		});
		socket.on(c.event.studentSendCoordinateEvent, function (data, fn) {
			sendCoordinateEv(socket, data, fn);
		});
		socket.on(c.event.studentEndPathEvent, function (data, fn) {
			endPathEV(socket, data, fn);
		});
		socket.on(c.event.studentErasePathEvent, function (data, fn) {});
		socket.on(c.event.studentUndoPathEvent, function (data, fn) {
			undoPathEv(socket, data, fn);
		});
		socket.on(c.event.studentClearCanvasEvent, function (data, fn) {
			clearCanvasEv(socket, data, fn);
		});
		socket.on(c.event.studentSetImageEvent, function (data, fn) {
			setImageEv(socket, data, fn);
		});
		socket.on(c.event.studentGetHistoryBackgroundEvent, function (data, fn) {
			loadLastBackgroundEv(socket, data, fn);
		});
		socket.on(c.event.studentLoadMemberInfoEvent, function (data, fn) {
			getGroupMemberInfosEv(socket, data, fn);
		});

		socket.on("disconnect", function () {
			exports.teacher.studentLeaveEvent(socket, {
				"userID": socket.userID,
				"userName": socket.userName,
				"groupID": socket.groupID
			});
			deleteUser(socket.id);
		});
	});
};

var requestInfoEv = function (socket, data, fn) {
	var userName = data.userName;
	var groupID = parseInt(data.groupID);
	enlistSocketToGroup(socket.id, groupID);
	socket.userName = userName;
	socket.groupID = groupID;
	console.log("socket requesting userID with userName " + userName);
	MongoUtil.getUserInfo(userName, function (err, userID) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::requestInfoEv::" + err);
			fn({"error": err,"id": ""});
			return;
		}
		console.log(userID);
		socket.userID = userID;
		enlistUser(socket.id, userID, userName);

		fn({"error":null, "id": userID});
		exports.teacher.studentJoinEvent(socket, {
			"userID": userID,
			"userName": userName,
			"groupID" : groupID
		});
	});
};

var startPathEv = function (socket, data, fn) {
	if (socket.state !== c.socketState.connected) {
		fn(c.EvAck.rejected);
		return;
	}
	socket.state = c.socketState.pathStarted;

	exports.teacher.studentHasStartedPath(socket, data);
	fn(c.EvAck.accepted);
};

var sendCoordinateEv = function (socket, data, fn) {
	if (socket.state !== c.socketState.pathStarted) {
		fn(c.EvAck.rejected);
		return;
	}
	var userID = data.userID;
	var userName = data.userName;
	var groupID = data.groupID;
	var path = data.path;
	console.log(userName + " sent a path " + path );
	MongoUtil.insertPath(userID, userName, groupID, path.pathID, path, function (err, pkg) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::sendCoordinateEv::" + err);
			fn(c.EvAck.rejected);
		}
		exports.teacher.studentSendCoordEvent(socket, pkg);
		fn(c.EvAck.accepted);
		var groupListMy = getGroupMembers(socket.id, groupID);
		for (var key in groupListMy) {
			if (DEBUG) console.log(groupListMy[key]);
			exports.students.socket(groupListMy[key]).emit(c.event.studentSyncNewPathEvent, pkg);
		}
		exports.teacher.studentSendCoordEvent(socket.id, pkg);
	});
};

var endPathEV = function (socket, data, fn) {
	switch (socket.state) {
		case c.socketState.pathStarted:
			exports.teacher.studentEndPathEvent(socket, data);
		break;
		case c.socketState.connected:
		default:
		break;
	}
	socket.state = c.socketState.connected;
	fn(c.EvAck.accepted);
};

var erasePathEv = function (socket, pathID, fn) {
};

var undoPathEv = function (socket, data, fn) {
	var groupID = data.groupID;
	var userID = data.userID;
	var userName = data.userName;
	var pathID = data.pathID;
	var sendPkg = {
		"groupID": groupID,
		"userID": userID,
		"userName": userName,
		"pathID": pathID
	};
	console.log(pathID);
	MongoUtil.undoPath(userID, userName, groupID, pathID, function (err) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::undoPathEv::" + err);
			fn("0");
			return;
		}
		var myGroup = getGroupMembers(socket.id, groupID);
		for (var key in myGroup) {
			if (DEBUG) console.log(myGroup[key]);
			exports.students.socket(myGroup[key]).emit(c.event.peerUndoPathEvent, sendPkg);
		}
		exports.teacher.studentUndoEvent(socket, sendPkg);
		fn("1");
	});
};

var clearCanvasEv = function (socket, data, fn) {
	var groupID = data.groupID;
	var userID = data.userID;
	var userName = data.userName;
	var myGroup = getGroupMembers(socket.id, groupID);
	MongoUtil.clearCanvas(userID, userName, groupID, function (err) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::" + err);
			fn("0");
			return;
		}
		for (var key in myGroup) {
			if (DEBUG) console.log(myGroup[key]);
			exports.students.socket(myGroup[key]).emit(c.event.peerClearCanvasEvent, {});
		}
		exports.teacher.studentClearEvent(socket, {
			"groupID": groupID,
			"userID": userID,
			"userName": userName
		});
		fn("1");
	});
};

var loadHistoryPathEv = function (socket, data, fn) {
	MongoUtil.loadHistory(data.groupID, function (err, result) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::loadHistoryPathEv::" + err);
			fn({});
			return;
		}
		fn(result);
	});
};

var loadLastBackgroundEv = function (socket, data, fn) {
	MongoUtil.getImageHistory(data.groupID, function (err, result) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::loadLastBackgroundEv::" + err);
			fn("0");
			return;
		}
		fn(result);
	});
};

var getGroupMemberInfosEv = function (socket, data, fn) {
	MongoUtil.getStudentList(parseInt(data.groupID), function (err, result) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::getGroupMemberInfosEv::" + err);
			fn({});
			return;
		}
		for (var i = 0; i < result.length; i++) {
			if (result[i].userName == socket.userName) {
				result.splice(i, 1);
				break;
			}
		}
		fn(result);
	});
};

var setImageEv = function (socket, data, fn) {
	MongoUtil.setImage(data.userID, data.userName, data.groupID, data.fileName, function (err) {
		if (err) {
			if (DEBUG) console.log("studentSocketHandler::setImageEv::" + err);
			// fn("0");
			return;
		}
		// fn("1");
		// console.log("in set image");
		var myGroup = getGroupMembers(socket.id, data.groupID);
		for (var key in myGroup) {
			if (DEBUG) console.log(myGroup[key]);
			exports.students.socket(myGroup[key]).emit(c.event.studentSetImageEvent, {
				"fileName": data.fileName
			});
		}
		exports.teacher.studentSetImageEvent(socket, {
			"groupID": data.groupID,
			"userID": data.userID,
			"userName": data.userName,
			"fileName": data.fileName
		});
	});
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////BIND TO EXIT EVENTS SO THE NODE CAN FINISH ALL OPS B4 QUIT////////////////////////////////////

function processExit(callback) {
	if (DEBUG) console.log("exit hook started");

	dumpAllUser(function () {
		callback();
	});
}

process.on('SIGINT', function() {
	processExit(function() {
		process.exit(0);
	});
});

process.on('SIGHUP', function() {
	processExit(function() {
		process.exit(0);
	});
});

process.on('SIGTERM', function() {
	processExit(function() {
		process.exit(0);
	});
});

process.on('SIGTSTP', function() {
	processExit(function() {
		process.exit(0);
	});
});
