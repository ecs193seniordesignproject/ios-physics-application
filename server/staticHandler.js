var MongoUtil = require('./MongoUtil.js');
var config = require('./config.json');
var DEBUG = config.DEBUG;

exports.getGroupList = function (callback) {
	MongoUtil.getGroupList(function (err, result) {
		if (err) {
			if (DEBUG) console.log("staticHandler::getGroupList::" + err);
		}
		callback(err, result);
	});
};

exports.getStudentList = function (groupID, callback) {
	MongoUtil.getStudentList(groupID, function (err, result) {
		if (err) {
			if (DEBUG) console.log("staticHandler::getGroupList::" + err);
		}
		callback(err, result);
	});
};
