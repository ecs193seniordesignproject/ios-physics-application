exports.event = {
	studentRequestInfoEvent: "stReqInfoEv",

	studentJoinEvent: "stJoinEv",
	studentLeaveEvent: "stLeaveEv",
	studentGetHistoryEvent: "stGetHisEv",
	studentGetHistoryBackgroundEvent: "stGetHisBgEv",
	studentLoadMemberInfoEvent: "stLdMrInfoEv",

	studentSetImageEvent : "stSetImgEv",

	studentStartPathEvent: "stStartPathEv",
	studentSendCoordinateEvent: "stSendCoordEv",
	studentEndPathEvent :"stEndPathEv",
	studentErasePathEvent: "stErasePathEv",
	studentUndoPathEvent: "stUndoPathEv",
	studentClearCanvasEvent: "stClCanvasEv",

	peerUndoPathEvent: "prUndoPathEv",
	peerClearCanvasEvent: "prClCanvasEv",

	studentSyncNewPathEvent: "stSyncNewPathEv",
	studentSyncDeletePathEvent: "stSyncDelPathEv",

	teacherSetUpGroupEvent: "thSetGroupEv",
	teacherLoadHistoryEvent: "thLdHisEv",
	teacherLoadLastBackgroundEvent: "thLdLsBgEv"
};

exports.socketState = {
	connected: 0,
	pathStarted: 1,
	disconnected: 2
};

exports.EvAck = {
	accepted: 0,
	rejected: 1
};