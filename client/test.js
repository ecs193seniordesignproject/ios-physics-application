var g_masterPaper;
var g_masterPathArray;
var g_masterDrawingBox;

var color = document.getElementById("color");
var colorStrHex = "#000";
color.onkeyup = function () {
    colorStrHex = this.value;
}

var linewidth = document.getElementById("lw");
var linewidthStr = "3";
linewidth.onkeyup = function () {
    linewidthStr = this.value;
}

function initDrawing() {
    var canvasCoordx = 20;
    var canvasCoordy = 20;
    
    var g_masterPaper = Raphael(canvasCoordx,canvasCoordy,700,500);
    
    var masterBackground = g_masterPaper.rect(0,0,700,500,10);
    masterBackground.attr("fill", "#eee");
    masterBackground.mousemove(function (event) {
        var evt = event;
        var IE = document.all?true:false;
        var x, y;
        if (IE) {
            x = evt.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            y = evt.clientY + document.body.scrollTop + document.documentElement.scrollTop;            
        }
        else {
            x = evt.pageX;
            y = evt.pageY;
        }
        this.ox = x - canvasCoordx;
        this.oy = y - canvasCoordy;
    });
    
    var start = function () {
        g_masterPathArray = new Array();
    },
        move = function (dx, dy) {
            if (g_masterPathArray.length == 0) {
                g_masterPathArray[0] = ["M",this.ox,this.oy];
                g_masterDrawingBox = g_masterPaper.path(g_masterPathArray);
                g_masterDrawingBox.attr("stroke", colorStrHex);
                g_masterDrawingBox.attr("stroke-width", linewidthStr);
            }
            else {
                g_masterPathArray[g_masterPathArray.length] = ["L",this.ox,this.oy];
            }
            g_masterDrawingBox.attr({path: g_masterPathArray});
        },
        end = function () {
            ; // don't do nuthin
        };
    
    masterBackground.drag(move,start,end);
    return g_masterPaper;
}
window.onload = function()
{
    var paper = initDrawing();
}