var studentNamespace = "/student";
var teacherNamespace = "/teacher";

var serverAddress = "http://localhost";
var serverPort = 8080;
//fallback setting for address and port
if (typeof G_configLoaded === 'undefined') {
	console.log("Warning: global config not present");
} else {
	serverAddress = global.serverAddress;
	serverPort = global.serverPort;
}

//Student client, callback trigger on connection established
function studentClient (userName, groupID, callback) {

	this.evListener = {};
	this.userID = "";
	this.userName = userName;
	this.groupID = groupID;
	this.valid = false;
	var query = "userName=" + userName + "&groupID=" + groupID;
	this.socket = io.connect(serverAddress + ":" + serverPort + studentNamespace, {
		"query": query,
		"sync disconnect on unload": true,
		"force new connection": true
	});

	var self = this;

	this.socket.on("connect", function () {
		console.log("socket connected");
		self.socket.emit(self.studentRequestInfoEvent, {
			"userName": self.userName,
			"groupID": self.groupID
		}, function (data) {
			console.log("response triggerred");
			console.log(data);
			if (data.err) {
				console.log(data.err);
				self.valid = false;
			} else {
				self.userID = data.id;
			}
			self.valid = true;
			self.loadHistory();
			self.loadLastBackground();
			self.loadMemberInfo();
		});
		if (callback) callback();
	});
	this.socket.on("error", function (reason) {
		self.valid = false;
		console.log("Socket connect failed: " + reason);
	});

	this.socket.on("disconnect", function () {
		self.valid = false;
		console.log("socket disconnected");
	});

	this.studentRequestInfoEvent = "stReqInfoEv";

	this.studentGetHistoryEvent = "stGetHisEv";
	this.studentGetHistoryBackgroundEvent = "stGetHisBgEv";
	this.studentLoadMemberInfoEvent = "stLdMrInfoEv";

	this.studentStartPathEvent = "stStartPathEv";
	this.studentSendCoordinateEvent = "stSendCoordEv";
	this.studentEndPathEvent = "stEndPathEv";
	this.studentErasePathEvent = "stErasePathEv";
	this.studentUndoPathEvent = "stUndoPathEv";
	this.studentClearCanvasEvent = "stClCanvasEv";
	this.peerUndoPathEvent = "prUndoPathEv";
	this.peerClearCanvasEvent = "prClCanvasEv";

	this.studentSyncNewPathEvent = "stSyncNewPathEv";
	this.studentSyncDeletePathEvent = "stSyncDelPathEv";

	this.studentSetImageEvent = "stSetImgEv";

	var binded = function (evName) {
		if (typeof self.evListener[evName] !== 'undefined') {
			return true;
		}
		return false;
	};

	this.socket.on(this.studentSyncNewPathEvent, function (data, ack) {
		console.log("trigger peer");
		var userID = data.userID;
		var groupID = data.groupID;
		var path = data.path;
		var width = data.width;
		var color = data.color;
		if (binded("peerDrawingArrive")) {
			self.evListener.peerDrawingArrive(data);
		} else {
			console.log("user " + userID + " of group " + groupID + " has drawn a path: " + path + " with width: " + width + " and color " + color);

		}
	});

	this.socket.on(this.peerUndoPathEvent, function (data, ack) {
		if (binded("peerUndo")) {
			self.evListener.peerUndo(data);
		} else {
			console.log("Peer just undoed a path");
		}
	});

	this.socket.on(this.peerClearCanvasEvent, function (data, ack) {
		if (binded("peerClear")) {
			self.evListener.peerClear();
		} else {
			console.log("Peer just cleared canvas");
		}
	});

	this.socket.on(this.studentSetImageEvent, function (data, ack) {
		var fileName = data.fileName;
		if (binded("peerSetFileName")) {
			self.evListener.peerSetFileName(fileName);
		} else {
			console.log("Peer just set file name: " + fileName);
		}
	});


	//Helper function
	this.uuidV4 = function () {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
		});
	};

	this.loadHistory = function () {
		this.socket.emit(this.studentGetHistoryEvent, {
			"groupID": this.groupID
		}, function (data) {
			if (binded("loadHistory")) {
				self.evListener.loadHistory(data);
			} else {
				console.log(data);
			}
		});
	};

	this.loadLastBackground = function () {
		this.socket.emit(this.studentGetHistoryBackgroundEvent, {
			"groupID": this.groupID
		}, function (data) {
			if (binded("loadLastBackground")) {
				self.evListener.loadLastBackground(data);
			} else {
				console.log("loadLastBackground not set");
				console.log(data);
			}
		});
	};

	this.loadMemberInfo = function () {
		console.log("loadMemberInfo running");
		this.socket.emit(this.studentLoadMemberInfoEvent, {
			"groupID": this.groupID
		}, function (data) {
			if (binded("loadMemberInfo")) {
				self.evListener.loadMemberInfo(data);
			} else {
				console.log("loadMemberInfo not set");
				console.log(data);
			}
		});
	};
}

studentClient.prototype.on = function (eventName, callback) {
	this.evListener[eventName] = callback;
};

studentClient.prototype.startPath = function () {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	this.socket.emit(this.studentStartPathEvent, {"userID": this.userID}, function (data) {
		console.log("startPath ack: " + data);
	});
};

studentClient.prototype.setFileWithFileName = function (fileName) {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	this.socket.emit(this.studentSetImageEvent, {
		"fileName": fileName,
		"userID": this.userID,
		"userName": this.userName,
		"groupID": this.groupID,
	});
	console.log("setted background");
};

studentClient.prototype.sendCoordinate = function (path, width, color) {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	console.log(path);
	this.socket.emit(this.studentSendCoordinateEvent, {
		"userID": this.userID,
		"userName": this.userName,
		"groupID": this.groupID,
		"path": path,
		"width": width,
		"color": color
	}, function (data) {
		console.log("server ack: " + data);
	});
};

studentClient.prototype.endPath = function (path, width, color) {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	this.socket.emit(this.studentEndPathEvent, {
		"userID": this.userID,
		"path": path,
		"width": width,
		"color": color
	}, function (data) {
		console.log("endPath ack: " + data);
	});
};

studentClient.prototype.undo = function (pathID) {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	this.socket.emit(this.studentUndoPathEvent, {
		"userID": this.userID,
		"userName": this.userName,
		"groupID": this.groupID,
		"pathID": pathID
	}, function (data) {
		console.log("undo ack: " + data);
	});
};

studentClient.prototype.clearCanvas = function () {
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	if (!this.valid) {
		console.log("Socket is not ready or connection is refused");
		return;
	}
	this.socket.emit(this.studentClearCanvasEvent, {
		"groupID": this.groupID
	}, function (data) {
		console.log("undo ack: " + data);
	});
};
