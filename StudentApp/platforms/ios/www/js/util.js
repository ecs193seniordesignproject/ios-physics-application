var uuidV4 = function () {
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		return v.toString(16);
	});
};

var getCanvasIndex = function (canvas, pathID) {
	if (!pathID) return -1;
	for (var i = 0; i < canvas.size(); i++) {
		if (canvas.item(i).pathID == pathID) return i;
	}
	return -1;
};