var staticUtil = (function () {
	var getGroupListPath = "/getGroupList";
	var getStudentListPath = "/getStudentList";
	var JQ = jQuery;
	var jQueryValid = function () {
		if (jQuery) {
			JQ = jQuery;
			return true;
		}
		return false;
	};
	var serverAddress = "http://localhost";
	var staticServerPort = 8080;
	//fallback setting for address and port
	if (typeof G_configLoaded === 'undefined') {
		console.log("Warning: global config not present");
	} else {
		serverAddress = global.serverAddress;
		staticServerPort = global.staticServerPort;
	}
	return {
		getGroupList: function (callback) {
			if (!jQueryValid()) {
				callback(new Error("Cannot find jQuery"), null);
			}
			var path = serverAddress + ":" + String(staticServerPort) + getGroupListPath;
			JQ.ajax({
				url: path,
				type: "GET",
				async:true,
				data: {},
				success: function(message) {
					callback(null, message);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					callback(new Error("ajax error: " + jqXHR.status + " " + errorThrown), null);
				}
			});
		},
		getStudentList: function (groupID, callback) {
			if (!jQueryValid()) {
				callback(new Error("Cannot find jQuery", null));
			}
			var path = serverAddress + ":" + String(staticServerPort) + getStudentListPath;
			JQ.ajax({
				url: path,
				type: "GET",
				async:true,
				data: {
					"groupID": parseInt(groupID)
				},
				success: function(message) {
					callback(null, message);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					callback(new Error("ajax error: " + jqXHR.status + " " + errorThrown), null);
				}
			});
		}
	};

})();
