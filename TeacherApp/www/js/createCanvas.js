// function findClosestMultipleOfThree(numberOfGroup) {
//         if (numberOfGroup > 0)
//             return Math.ceil(numberOfGroup / 3.0) * 3;
//         else if (numberOfGroup < 0)
//             return Math.floor(numberOfGroup / 3.0) * 3;
//         else
//             return 3;
// }

function createCanvas(numberOfGroup) {
    // var numOfRows = findClosestMultipleOfThree(numberOfGroup) / 3;
    var alphabetArray = ['z', 'y', 'x', 'w', 'v',
                         'u', 't', 's', 'r', 'q',
                         'p', 'o', 'n', 'm', 'l',
                         'k', 'j', 'i', 'h', 'g',
                         'f', 'e', 'd', 'c', 'b', 'a'];
    // var numOfColumn = 3;
    var canvasHtmlString = "<table id='myCanvas style='border-spacing: 0; border-collapse: collapse; '>";
    canvasHtmlString += "<tr>";
    for (var group = 0; group < numberOfGroup; group++) {
        // canvasHtmlString += "<tr>";
        // for (var column = 0; column < numOfColumn; column++) {
        canvasHtmlString += "<td>";
        canvasHtmlString += "<canvas id='";
        canvasHtmlString += alphabetArray.pop();
        canvasHtmlString += "' style='border:0px solid #aaa;'></canvas>";
        canvasHtmlString += "</td>";
        // }
        // canvasHtmlString += "</tr>";
    }
    canvasHtmlString += "</tr>";
    canvasHtmlString += "</table>";

    return canvasHtmlString;
}

function fillJsonArray(numberOfGroups) {
    var alphabetArray = ['z', 'y', 'x', 'w', 'v',
                         'u', 't', 's', 'r', 'q',
                         'p', 'o', 'n', 'm', 'l',
                         'k', 'j', 'i', 'h', 'g',
                         'f', 'e', 'd', 'c', 'b', 'a'];
    var jsonArray = new Array();
    for (var i = 0; i < numberOfGroups; i++) {
        jsonArray[i] = alphabetArray.pop();
    }

    return jsonArray;
}
