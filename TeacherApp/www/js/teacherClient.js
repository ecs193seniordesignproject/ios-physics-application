var studentNamespace = "/student";
var teacherNamespace = "/teacher";

var serverAddress = "http://localhost";
var serverPort = 8080;
//fallback setting for address and port
if (typeof G_configLoaded === 'undefined') {
	console.log("Warning: global config not present");
} else {
	serverAddress = global.serverAddress;
	serverPort = global.serverPort;
}

function teacherClient (callback) {
	this.socket = io.connect(serverAddress + ":" + serverPort + teacherNamespace);

	this.studentStartPathEvent = "stStartPathEv";
	this.studentSendCoordinateEvent = "stSendCoordEv";
	this.studentEndPathEvent = "stEndPathEv";
	this.teacherSetUpGroupEvent =  "thSetGroupEv";
	this.studentUndoPathEvent = "stUndoPathEv";
	this.studentClearCanvasEvent = "stClCanvasEv";

	this.studentSetImageEvent = "stSetImgEv";

	this.studentJoinEvent = "stJoinEv";
	this.studentLeaveEvent = "stLeaveEv";
	this.teacherLoadHistoryEvent = "thLdHisEv";
	this.teacherLoadLastBackgroundEvent = "thLdLsBgEv";

	this.studentStartEvHandler = null;
	this.studentSendCoordEvHandler = null;
	this.studentEndPathEvHandler = null;
	this.studentSetImageEventHandler = null;
	this.studentUndoPathEvHandler = null;
	this.studentClearCanvasEvHandler = null;
	this.studentJoinEvHandler = null;
	this.studentLeaveEvHandler = null;
	this.teacherLoadHistoryEvHandler = null;

	var self = this;

	this.socket.on("connect", function () {
		if (DEBUG > 1)
			console.log("teacher socket connected");
		if (callback) callback();
	});

	this.socket.on(this.studentJoinEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has join: " + JSON.stringify(data));
		if (self.studentJoinEvHandler) self.studentJoinEvHandler(data);
	});
	this.socket.on(this.studentLeaveEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has left: " + JSON.stringify(data));
		if (self.studentLeaveEvHandler) self.studentLeaveEvHandler(data);
	});

	this.socket.on(this.studentStartPathEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has started path: " + JSON.stringify(data));
		if (self.studentStartEvHandler) self.studentStartEvHandler(data);
	});
	this.socket.on(this.studentSendCoordinateEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has sent coordinate: " + JSON.stringify(data));
		if (self.studentSendCoordEvHandler) self.studentSendCoordEvHandler(data);
	});
	this.socket.on(this.studentSetImageEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has set iamge as background" + JSON.stringify(data));
		if (self.studentSetImageEventHandler) self.studentSetImageEventHandler(data);
	});
	this.socket.on(this.studentEndPathEvent, function (data, ack) {
		if (DEBUG > 1)
			console.log("student has ended path: " + JSON.stringify(data));
		if (self.studentEndPathEvHandler) self.studentEndPathEvHandler(data);
	});
	this.socket.on(this.studentUndoPathEvent, function (data, ack) {
		if (DEBUG > 1) {
			console.log("student has undo");
			console.log(data);
		}
		if(self.studentUndoPathEvHandler) self.studentUndoPathEvHandler(data);
		else {
			console.log("not binded");
		}
	});
	this.socket.on(this.studentClearCanvasEvent, function (data, ack) {
		if (DEBUG > 1) {
			console.log("student has clear");
			console.log(data);
		}
		if(self.studentClearCanvasEvHandler) self.studentClearCanvasEvHandler(data);
		else console.log("not binded");
	});
	this.socket.on(this.teacherLoadHistoryEvent, function (data, ack) {
		if (self.teacherLoadHistoryEvHandler) self.teacherLoadHistoryEvHandler(data);
		else console.log(data);
	});
}

teacherClient.prototype.on = function (evName, fn) {
	switch(evName) {
		case this.studentJoinEvent:
		this.studentJoinEvHandler = fn;
		break;
		case this.studentLeaveEvent:
		this.studentLeaveEvHandler = fn;
		break;
		case this.studentStartPathEvent:
		this.studentStartEvHandler = fn;
		break;
		case this.studentSendCoordinateEvent:
		this.studentSendCoordEvHandler = fn;
		break;
		case this.studentEndPathEvent:
		this.studentEndPathEvHandler = fn;
		break;
		case this.studentUndoPathEvent:
		this.studentUndoPathEvHandler = fn;
		break;
		case this.studentClearCanvasEvent:
		this.studentClearCanvasEvHandler = fn;
		break;
		case this.studentSetImageEvent:
		this.studentSetImageEventHandler = fn;
		break;
		case this.teacherLoadHistoryEvent:
		this.teacherLoadHistoryEvHandler = fn;
		break;
		default:
		console.log("Unrecognized ev: " + evName);
		break;
	}
};

teacherClient.prototype.addGroup = function (groupID, groupName, fn) {
	this.socket.emit(this.teacherSetUpGroupEvent, {

	});
};

teacherClient.prototype.getLastBackground = function (fn) {
	this.socket.emit(this.teacherLoadLastBackgroundEvent, {}, function (data) {
		if (fn) fn(data);
		else console.log(data);
	});
};
